
#Project Management in the Public Sector: Success and Failure Patterns Compared to Private Sector Projects
##Citing
ArticleAuthorRef: (Bruce N. Baker@WILEY;a ZoaArticleReference)8

##Authors
Auhor Name: N. Baker, Bruce
Auhor ID: Bruce N. Baker@WILEY; 
Auhor Affiliations:  SRI International, USA; 
Auhor Name: Fisher, Dalmar
Auhor ID: Dalmar Fisher@WILEY; 
Auhor Affiliations:  Boston College, USA; 
Auhor Name: C. Murphy, David
Auhor ID: David C. Murphy@WILEY; 
Auhor Affiliations:  Boston College, USA; 

##Abstract
Summary This chapter contains sections titled: Introduction Determinants of Cost and Schedule Overrun Comparisons between Private Sector and Public Sector Projects Strategies for Overcoming some of...
##Keywords
technical performance and satisfaction;average cost growth;environmental cleanup efforts;coordination and relation patterns;cost‐plus‐fixed fee contracts

##References
###This article is cited by
 Roland J. Cole, Jennifer A. Kurtz, Isabel A. Cole, Implementing Public Fiber-to-the-Home Network Projects, Managing E-Government Projects, 10.4018/978
###This article Cites

##Article Metadata
pdfUrl nil
pages an OrderedCollection(920 921 922 923 924 925 926 927 928 929 930 931 932 933 934)
booktitle Project Management Handbook
Language en
AbstractUrl https://onlinelibrary.wiley.com/doi/abs/10.1002/9780470172353.ch36
