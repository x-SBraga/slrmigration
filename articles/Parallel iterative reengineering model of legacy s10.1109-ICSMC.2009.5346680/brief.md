
#Parallel iterative reengineering model of legacy systems
##Citing
ArticleAuthorRef: (37667985700@IEEE;a ZoaArticleReference)9

##Authors
Auhor Name: Su, Xing
Auhor ID: 37667985700@IEEE; 
Auhor Affiliations:  
Auhor Name: Yang, Xiaohu
Auhor ID: 37275558300@IEEE; 
Auhor Affiliations:  
Auhor Name: Li, Juefeng
Auhor ID: 37676292600@IEEE; 
Auhor Affiliations:  
Auhor Name: Wu, Di
Auhor ID: 37673574100@IEEE; 
Auhor Affiliations:  

##Abstract
Great improvements have been made in methodologies on reengineering of legacy systems to enhance the migration process. Unfortunately, taking into account the large scale of these systems, the duration of reengineering process can still be quite long which will make an impact on the business the systems supported. In this paper we propose a parallel iterative reengineering model to reduce the reengineering process duration. The model uses the method of formal concept analysis to process the complex access relationship between legacy components and shared data. An algorithm named ¿bottom-up¿, which consists of two parts, is introduced to build the parallel schedule for the reengineering process. This model shortens the reengineering duration and has been proven to be successful in improving the transition system's performance in a comparative experiment.
##Keywords
bottom-up algorithm;iterative methods;Educational institutions;Formal Concept Analysis;Iterative methods;complex access relationship;reengineering process duration;Costs;migration;USA Councils;parallel iterative reengineering model;legacy systems;Bottom-Up Algorithm;Cybernetics;Performance;System performance;Legacy System Reengineering;legacy components;Parallel Iterative Reengineering Model;Computer science;Business process re-engineering;Iterative algorithms;Large-scale systems;formal concept analysis;software maintenance;shared data


##Overview
The article proposes to uses paralellism on analysis tools for reengineering. 

The article focuses on the scheduling of reengineering tasks, basing the choices on the data reengineering. 
The idea is to use Formal Concept Analysis to clusterize the tasks, reducing as possible the required cycles to  finish, maximizing parallelism.
 

##References
###This article is cited by
###This article Cites
 A. Bianchi, D. Caivano, and G. Visaggio,
 A. Bianchi, D. Caivano, V. Marengo, and 
 A. Bianchi, Danilo Caivano, Vittorio Mar
 B. Wu, D. Lawless, J. Bisbal, R. Richard
 Juefeng Li, Xiaohu Yang, Zhijun He, Usin
 Juefeng Li, Xiaohu Yang, Tao Huan, Zhiju
 M. Brodie and M. Stonebraker, Migrating 
 Garret Birkhoff. Lattice Theory, America
 Bernhard Ganter and Rudolf Wille, Formal
 Igor Ivkovic, Kostas Kontogiannis, Using
 Gabriela Arevalo, High Level Views in Ob

##Article Metadata
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=5340904
isNow false
subType IEEE Conference
purchaseOptions a Dictionary('mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfo'->an OrderedCollection(a Dictionary('memberPrice'->'$14.95' 'nonMemberPrice'->'$33.00' 'partNumber'->'5346680' 'type'->'PDF/HTML' )) 'pdfPricingInfoAvailable'->true 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
conferenceDate 11-14 Oct. 2009
pubTopics an OrderedCollection(a Dictionary('name'->'Computing and Processing' ) a Dictionary('name'->'Robotics and Control Systems' ) a Dictionary('name'->'Signal Processing and Analysis' ) a Dictionary('name'->'Power, Energy and Industry Applications' ))
pdfUrl nil
isNumber 5345886
isPromo false
startPage 4054
isEphemera false
isACM false
isSAE false
mlTime PT0.050546S
isMorganClaypool false
sourcePdf 06954054.pdf
isFreeDocument false
isDynamicHtml true
pdfPath /iel5/5340904/5345886/05346680.pdf
articleId 5346680
isMarketingOptIn false
isOpenAccess false
isSMPTE false
chronOrPublicationDate 11-14 Oct. 2009
isEarlyAccess false
isnn an OrderedCollection(a Dictionary('format'->'Print ISSN' 'value'->'1062-922X' ))
mediaPath /mediastore_new/IEEE/content/media/5340904/5345886/5346680
endPage 4058
isStaticHtml true
confLoc San Antonio, TX, USA
isGetAddressInfoCaptured false
content_type Conferences
_value IEEE
htmlLink /document/5346680/
accessionNumber 11004363
html_flag true
lastupdate 2020-02-06
pubLink /xpl/conhome/5340904/proceeding
isChapter false
isStandard false
isCustomDenial false
publicationYear 2009
htmlAbstractLink /document/5346680/
issueLink /xpl/tocresult.jsp?isnumber=5345886
displayPublicationTitle 2009 IEEE International Conference on Systems, Man and Cybernetics
isGetArticle false
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'false' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'true' 'footnotes'->'false' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
ml_html_flag true
openAccessFlag F
isJournal false
publicationNumber 5340904
dbTime 4 ms
rightsLinkFlag 1
contentType conferences
isBookWithoutChapters false
xploreDocumentType Conference Publication
chronDate 11-14 Oct. 2009
isProduct false
metrics a Dictionary('citationCountPaper'->0 'citationCountPatent'->0 'totalDownloads'->160 )
ephemeraFlag false
allowComments false
formulaStrippedArticleTitle Parallel iterative reengineering model of legacy systems
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=Parallel+iterative+reengineering+model+of+legacy+systems&isbn=978-1-4244-2793-2&publicationDate=Oct.+2009&author=Xing+Su&ContentID=10.1109/ICSMC.2009.5346680&orderBeanReset=true&startPage=4054&endPage=4058&proceedingName=2009+IEEE+International+Conference+on+Systems%2C+Man+and+Cybernetics
publicationTitle 2009 IEEE International Conference on Systems, Man and Cybernetics
isbn an OrderedCollection(a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'978-1-4244-2793-2' ) a Dictionary('format'->'CD' 'isbnType'->'' 'value'->'978-1-4244-2794-9' ))
xplore-pub-id 5340904
xplore-issue 5345886
isConference true
displayDocTitle Parallel iterative reengineering model of legacy systems
doiLink https://doi.org/10.1109/ICSMC.2009.5346680
isOUP false
isNotDynamicOrStatic false
isBook false
getProgramTermsAccepted false
articleNumber 5346680
