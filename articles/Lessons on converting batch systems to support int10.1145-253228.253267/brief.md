
#Lessons on converting batch systems to support interaction: experience report
##Citing
ArticleAuthorRef: (81350602833@ACM;a ZoaArticleReference)997

##Authors
Auhor Name: DeLine, Robert
Auhor ID: 81350602833@ACM; 
Auhor Affiliations:  Computer Science Department, Carnegie Mellon University, 5000 Forbes Avenue, Pittsburgh, PA; 
Auhor Name: Zelesnik, Gregory
Auhor ID: 81342517413@ACM; 
Auhor Affiliations:  Computer Science Department, Carnegie Mellon University, 5000 Forbes Avenue, Pittsburgh, PA; 
Auhor Name: Shaw, Mary
Auhor ID: 81339528004@ACM; 
Auhor Affiliations:  Computer Science Department, Carnegie Mellon University, 5000 Forbes Avenue, Pittsburgh, PA; 

##Abstract
ABSTRACT No abstract available.
##Keywords
software evolution;interactive systems;reengineering;batch systems
##Overview
The article exposes the challenges of the transformation of a software from batch to interactive. The project exhibits all the problematic of migration: data, function and interaction. ### Conversion Issues- Assumptions about execution duration	A batch starts processes and finish.	An interactive program since it requires human feedback may stay stationary for indeterminated amount time.	- Incremental processing	A batch computes everything each time is executed. Not internal data persistence is required. 	An interactive program allows incremental edition.	 - Partial processing	A batch expects complete input and it produces complete output.	An interactive program allows users to create and modify. - Scope of processing	A batch typically provides a fixed computation over narrow input. 	An interative program may support many works to be active at the same time.	- Unordered and repeated processing	A batch is designed knowing the order of each phase. This fact allows to make many assumptions. 	An interactive program process is defined by the user, as well as combination and order of the work. 	- Allowed operations	A batch is designed to handle a fixed input during one execution. 	An interactive program require support to create, add, modify, deletion of both inputs and produced products. 	- Error prevention versus error detection	A batch system can only report errors that are found in its input. 	Interactive applications are designed to prevent ill-formed contructs.- Error reporting	A batch system can only report errors found in the input as soon as it happens on the Error output.	An interactive system reports the errors when is convenient for the user experience. 	- System control	A batch system, in the case of  a severe failure, can choose to fully terminate the process.	An interactive system must recover from failure, and allow the user to keep on working. 
##Advices
The paper has 8 advices to manage this kind of project. - External behavior 1. Choose a style of interaction that fits your problem   most of the paper is based on moving to graphical interaction. It may not be the case always.2. Add new operations if necessary   Add only the required operations. Follow the same architecture than in the original application (homonegeity) 3. Replace as many error reports with checks that ward off the error.  Check error reports to be sure they will be meaningful in an interactive setting   Errors handling must change. Ensure that the message make sense in the context of an interactive application- Internal structrue4. Decide on the approapiate relation between the user interface and the former batch program.     Analyse coupling and timing. 5. Restructure the batch system as necessary to reduce the granularity of the operations    Refactor to recognice phases and move them into individually callable functions.      Mind the assumptions.6. Identify and isolate the system state   Global variables must be isolated. Ensure that they are usable for reentrant usage. - Code7. Make the assumptions of the operation explicit.       Identify invariants of data structures, the order and context assumptions on the processes. Make them explicit. 8. Be sure you do complete tstorage management      Ensure that the intermediate data structures are properly released (free).

##References
###This article is cited by
 Proceedings of the 1999 symposium on Software reusability (10.1145/303008.303023)
 Proceedings of the Sixth International IEEE Conference on Commercial-off-the-Shelf (COTS)-Based Software Systems (10.1109/ICCBSS.2007.2)
 Proceedings of the 21st international conference on Software engineering (10.1145/302405.302456)
 Avoiding Packaging Mismatch with Flexible Packaging (10.1109/32.908958)
###This article Cites
 A.N. Habermann and D. Notkin. "Gandalf: 
 T. Lane, "User Interface Software Struct
 R.W. Sheifler and J. Gettys. "The X wind
 D.E. Mularz. "Pattern-based integration 
 S.P. Reiss. "Connecting tools using mess
 A.V. Aho, R. Sethi, and J. D. Ullman. "C
 R. Taylor and G. Johnson. "Separation of
 M.E. Lesk. "Lex - A Lexical Analyzer Gen
 T. Teitelbaum and T. Reps. "The Cornell 
 S.C. Johnson. "Yacc - Yet Another Compil
 M. Shaw, R. DeLine, D. V. Klein, T. L. R
 V. Ambriola and D. Notkin. "Reasoning ab

##Article Metadata
EventType PAPER_CONFERENCE
PublisherCity New York, NY, USA
pages an OrderedCollection(195)
EventCity Boston, Massachusetts, USA
CollectionTitle ICSE '97
NumberOfPages 10
isbn 0897919149
