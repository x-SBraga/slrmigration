
#Extreme maintenance: Transforming Delphi into C#
##Citing
ArticleAuthorRef: (37268800500@IEEE;a ZoaArticleReference)10

##Authors
Auhor Name: Brant, John
Auhor ID: 37268800500@IEEE; 
Auhor Affiliations:  
Auhor Name: Roberts, Don
Auhor ID: 37279919900@IEEE; 
Auhor Affiliations:  
Auhor Name: Plendl, Bill
Auhor ID: 37590906700@IEEE; 
Auhor Affiliations:  
Auhor Name: Prince, Jeff
Auhor ID: 37601089000@IEEE; 
Auhor Affiliations:  

##Abstract
Sometimes projects need to switch implementation languages. Rather than stopping software development and rewriting the project from scratch, transformation rules can map from the original language to the new language. These rules can be developed in parallel to standard software development, allowing the project to be cut over without any loss of development time, once the rules are complete. This paper presents a migration project that used transformation rules to successfully convert 1.5 MLOC of Delphi to C# in 18 months while allowing existing Delphi development to continue.
##Keywords
1.5 MLOC;Switches;Pattern matching;Programming;Libraries;Indexing;implementation languages switching;Delphi;software maintenance;Graphical user interfaces;Software;standard software development;C#;transformation rules;C language

##References
###This article is cited by
 Benoît Verhaeghe, Anne Etien, Nicolas Anquetil, Abderrahmane Seriai, Laurent Deruelle, Stéphane Ducasse, Mustapha Derras, "GUI Migration using MDE fro
 Jason Lecerf, John Brant, Thierry Goubier, Stéphane Ducasse, "A Reflexive and Automated Approach to Syntactic Pattern Matching in Code Transformations
###This article Cites
 G. Arango, I. Baxter, P. Freeman, and C.
 W. Loew-Blosser, "Transformation of an a
 I. D. Baxter, C. Pidgeon, and M. Mehlich
 J. Spolsky, Joel on Software: And on Div
 J. R. Cordy, "The TXL source transformat
 M. Bravenboer, K. T. Kalleberg, R. Verma
 D. Roberts and J. Brant, "Tools for maki
 D. Roberts, J. Brant, and R. Johnson, "A

##Article Metadata
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=5604771
isNow false
subType IEEE Conference
purchaseOptions a Dictionary('mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfo'->an OrderedCollection(a Dictionary('memberPrice'->'$14.95' 'nonMemberPrice'->'$33.00' 'partNumber'->'5609731' 'type'->'PDF/HTML' )) 'pdfPricingInfoAvailable'->true 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
conferenceDate 12-18 Sept. 2010
pubTopics an OrderedCollection(a Dictionary('name'->'Computing and Processing' ))
pdfUrl nil
isNumber 5609528
isPromo false
startPage 1
isEphemera false
isACM false
isSAE false
mlTime PT0.043008S
isMorganClaypool false
sourcePdf BrantExtremeCameraReady.pdf
isFreeDocument false
isDynamicHtml true
pdfPath /iel5/5604771/5609528/05609731.pdf
articleId 5609731
isMarketingOptIn false
isOpenAccess false
isSMPTE false
chronOrPublicationDate 12-18 Sept. 2010
isEarlyAccess false
isnn an OrderedCollection(a Dictionary('format'->'Print ISSN' 'value'->'1063-6773' ) a Dictionary('format'->'Electronic ISSN' 'value'->'1063-6773' ))
mediaPath /mediastore_new/IEEE/content/media/5604771/5609528/5609731
endPage 8
isStaticHtml true
confLoc Timisoara, Romania
isGetAddressInfoCaptured false
content_type Conferences
_value IEEE
htmlLink /document/5609731/
accessionNumber 11599166
html_flag true
lastupdate 2020-06-20
pubLink /xpl/conhome/5604771/proceeding
isChapter false
isStandard false
isCustomDenial false
publicationYear 2010
htmlAbstractLink /document/5609731/
issueLink /xpl/tocresult.jsp?isnumber=5609528
displayPublicationTitle 2010 IEEE International Conference on Software Maintenance
isGetArticle false
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'true' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'true' 'footnotes'->'true' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
ml_html_flag true
openAccessFlag F
isJournal false
publicationNumber 5604771
dbTime 2 ms
rightsLinkFlag 1
contentType conferences
isBookWithoutChapters false
xploreDocumentType Conference Publication
chronDate 12-18 Sept. 2010
isProduct false
metrics a Dictionary('citationCountPaper'->2 'citationCountPatent'->0 'totalDownloads'->135 )
citationCount 2
ephemeraFlag false
allowComments false
formulaStrippedArticleTitle Extreme maintenance: Transforming Delphi into C#
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=Extreme+maintenance%3A+Transforming+Delphi+into+C%23&isbn=978-1-4244-8630-4&publicationDate=Sept.+2010&author=John+Brant&ContentID=10.1109/ICSM.2010.5609731&orderBeanReset=true&startPage=1&endPage=8&proceedingName=2010+IEEE+International+Conference+on+Software+Maintenance
publicationTitle 2010 IEEE International Conference on Software Maintenance
isbn an OrderedCollection(a Dictionary('format'->'Electronic ISBN' 'isbnType'->'' 'value'->'978-1-4244-8629-8' ) a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'978-1-4244-8630-4' ) a Dictionary('format'->'Online ISBN' 'isbnType'->'' 'value'->'978-1-4244-8628-1' ))
xplore-pub-id 5604771
xplore-issue 5609528
isConference true
displayDocTitle Extreme maintenance: Transforming Delphi into C#
doiLink https://doi.org/10.1109/ICSM.2010.5609731
isOUP false
isNotDynamicOrStatic false
isBook false
getProgramTermsAccepted false
articleNumber 5609731
