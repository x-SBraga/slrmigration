
#Knowledge-based user interface migration
##Citing
ArticleAuthorRef: (37270392400@IEEE;a ZoaArticleReference)2

##Authors
Auhor Name: Moore, M.M.
Auhor ID: 37270392400@IEEE; 
Auhor Affiliations:  
Auhor Name: Rugaber, Rugaber
Auhor ID: 37268831900@IEEE; 
Auhor Affiliations:  
Auhor Name: Seaver, Seaver
Auhor ID: 37662164700@IEEE; 
Auhor Affiliations:  

##Abstract
A significant problem in reengineering large systems is adapting the user interface to a new environment. Often, drastic changes in the user interface are inevitable, as in migrating a text-based system to a workstation with graphical user interface capabilities. This experience report chronicles a study of user interface migration issues, examining and evaluating current tools and techniques. It also describes a case study under taken to explore the use of knowledge engineering to aid in migrating interfaces across platforms.<
<ETX xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xlink="http://www.w3.org/1999/xlink">&gt</ETX>
##Keywords
knowledge engineering;software portability;knowledge-based user interface migration;Software maintenance;text-based system;Intelligent systems;User interfaces;systems reengineering;software maintenance;knowledge based systems;software techniques;Computer-aided software engineering;user interfaces;software tools;workstation;Software design/development;reverse engineering;graphical user interface
##Overview
The article is financial article. It reviews all the failures on UK Public IT administration modernization. It remarks the requirement of proper management. The management is in concurrence with private sector management.the paper explains many cases of failure with reasons and audits. 

##References
###This article is cited by
 Ying Zou, Qi Zhang, Xulin Zhao, "Improving the Usability of E-Commerce Applications using Business Processes", (https://scholar.google.com/scholar?as_
 D. Bovenzi, G. Canfora, A.R. Fasolino, "Enabling legacy system accessibility by Web heterogeneous clients", (https://scholar.google.com/scholar?as_q=E
 G. Canfora, A. Cimitile, A. De Lucia, G.A. Di Lucca, "Decomposing legacy programs: a first step towards migrating to client-server platforms", (https:
 E. Stroulia, M. El-Ramly, P. Sorenson, "From legacy to Web through interaction modeling", (https://scholar.google.com/scholar?as_q=From+legacy+to+Web+
 Andres Kull, "Automatic GUI Model Generation: State of the Art", (https://scholar.google.com/scholar?as_q=Automatic+GUI+Model+Generation%3A+State+of+t
 A. Strlzoff, L. Petzold, "Deriving user interface requirements from densely interleaved scientific computing applications", (https://scholar.google.co
 A. Panteleymonov, "Interoperable thin client separation from GUI applications", (https://scholar.google.com/scholar?as_q=Interoperable+thin+client+sep
 G. Antoniol, R. Fiutem, E. Merlo, P. Tonella, "Application and user interface migration from BASIC to Visual C++", (https://scholar.google.com/scholar
 M. Moore, S. Rugaber, "Using knowledge representation to understand interactive systems", (https://scholar.google.com/scholar?as_q=Using+knowledge+rep
 Benoît Verhaeghe, Anne Etien, Nicolas Anquetil, Abderrahmane Seriai, Laurent Deruelle, Stéphane Ducasse, Mustapha Derras, "GUI Migration using MDE fro
 M. El-Ramly, P. Iglinski, E. Stroulia, P. Sorenson, B. Matichuk, "Modeling the system-user dialog using interaction traces", (https://scholar.google.c
###This article Cites
 KnowledgeWorker System Version 1.60 User
 D. Heller, Motif Programming Manual, O'R
 , "College of Computing", (https://schol
 M. Little, The Simple User Interface Too
 Van Sickle, L. Larry, Z. Yang and M. Bal
 S. Meyer, R. Oberg and D. Walton, XVT Te
 G. Atango and R. Prieto-Diaz, Domain Ana
 M. Moore, S. Rugaber and H. Astudillo, (
 E. Merlo, J.F. Girard, K. Kontogiannis, 
 R. J. Brachman, D. L. McGuiness, P. Pate
 M. Wagner, "New IXI Software Will Give M
 B. E. Rector, Developing Windows 3.1 App
 H. Baldwin, "Open Look to Motif Converte
 J. Foley, W. C. Kim, S. Kovacevic and K.
 "Partner and Liken Provide Interoperabil
 L. M. Wills, "Automated Program Recognit

##Article Metadata
isPromo false
pdfPath /iel2/992/7911/00336788.pdf
isMarketingOptIn false
doiLink https://doi.org/10.1109/ICSM.1994.336788
isProduct false
isFreeDocument false
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'true' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'false' 'footnotes'->'false' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
contentType conferences
accessionNumber 4785191
rightsLinkFlag 1
content_type Conferences
citationCount 16
metrics a Dictionary('citationCountPaper'->16 'citationCountPatent'->1 'totalDownloads'->79 )
isEphemera false
isBook false
mlTime PT0.02537S
chronDate 19-23 Sep 1994
openAccessFlag F
isCustomDenial false
allowComments false
htmlAbstractLink /document/336788/
endPage 79
isStaticHtml false
isbn an OrderedCollection(a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'0-8186-6330-8' ))
isGetArticle false
isChapter false
isMorganClaypool false
pdfUrl nil
isJournal false
isConference true
dbTime 2 ms
pubLink /xpl/conhome/992/proceeding
pubTopics an OrderedCollection(a Dictionary('name'->'Computing and Processing' ))
publicationNumber 992
isNotDynamicOrStatic true
articleNumber 336788
isEarlyAccess false
issueLink /xpl/tocresult.jsp?isnumber=7911
ml_html_flag false
publicationYear 1994
isOUP false
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=Knowledge-based+user+interface+migration&isbn=0-8186-6330-8&publicationDate=1994&author=Moore&ContentID=10.1109/ICSM.1994.336788&orderBeanReset=true&startPage=72&endPage=79&proceedingName=Proceedings+1994+International+Conference+on+Software+Maintenance
publicationTitle Proceedings 1994 International Conference on Software Maintenance
sourcePdf 00336788.pdf
isGetAddressInfoCaptured false
xploreDocumentType Conference Publication
isSMPTE false
articleId 336788
isOpenAccess false
lastupdate 2020-10-17
isNow false
html_flag false
displayPublicationTitle Proceedings 1994 International Conference on Software Maintenance
conferenceDate 19-23 Sept. 1994
isBookWithoutChapters false
subType IEEE Conference
purchaseOptions a Dictionary('mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfo'->an OrderedCollection(a Dictionary('memberPrice'->'$14.95' 'nonMemberPrice'->'$33.00' 'partNumber'->'336788' 'type'->'PDF' )) 'pdfPricingInfoAvailable'->true 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
chronOrPublicationDate 19-23 Sep 1994
confLoc Victoria, BC, Canada, Canada
isACM false
isStandard false
startPage 72
displayDocTitle Knowledge-based user interface migration
getProgramTermsAccepted false
ephemeraFlag false
isNumber 7911
isDynamicHtml false
formulaStrippedArticleTitle Knowledge-based user interface migration
isSAE false
mediaPath 
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=992
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
