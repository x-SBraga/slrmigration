
#Migrating Legacy Software to the Cloud with ARTIST
##Citing
ArticleAuthorRef: (38598212800@IEEE;a ZoaArticleReference)13

##Authors
Auhor Name: Bergmayr, Alexander
Auhor ID: 38598212800@IEEE; 
Auhor Affiliations:  
Auhor Name: Brunelière, Hugo
Auhor ID: 38193710500@IEEE; 
Auhor Affiliations:  
Auhor Name: Izquierdo, Javier Luis Cánovas
Auhor ID: 37408477200@IEEE; 
Auhor Affiliations:  
Auhor Name: Gorroñogoitia, Jesús
Auhor ID: 38656888000@IEEE; 
Auhor Affiliations:  
Auhor Name: Kousiouris, George
Auhor ID: 37546008500@IEEE; 
Auhor Affiliations:  
Auhor Name: Kyriazis, Dimosthenis
Auhor ID: 37399757400@IEEE; 
Auhor Affiliations:  
Auhor Name: Langer, Philip
Auhor ID: 37304034500@IEEE; 
Auhor Affiliations:  
Auhor Name: Menychtas, Andreas
Auhor ID: 37659554600@IEEE; 
Auhor Affiliations:  
Auhor Name: Orue-Echevarria, Leire
Auhor ID: 38648274000@IEEE; 
Auhor Affiliations:  
Auhor Name: Pezuela, Clara
Auhor ID: 38641929800@IEEE; 
Auhor Affiliations:  
Auhor Name: Wimmer, Manuel
Auhor ID: 37297155200@IEEE; 
Auhor Affiliations:  

##Abstract
As cloud computing allows improving the quality of software and aims at reducing costs of operating software, more and more software is delivered as a service. However, moving from a software as a product strategy to delivering software as a service hosted in cloud environments is very ambitious. This is due to the fact that managing software modernization is still a major challenge, especially when paradigm shifts, such as moving to cloud environments, are targeted that imply fundamental changes to how software is modernized, delivered, and sold. Thus, in addition to technical aspects, business aspects need also to be considered. ARTIST proposes a comprehensive software modernization approach covering business and technical aspects. In particular, ARTIST employs Model-Driven Engineering (MDE) techniques to automate the reverse engineering of legacy software and forward engineering of cloud-based software in a way that modernized software truly benefits from targeted cloud environments. Therewith, ARTIST aims at reducing the risks, time, and costs of software modernization and lowers the barriers to exploit cloud computing capabilities and new business models.
##Keywords
Unified modeling language;Software as a service;software quality;business aspect;software quality improvement;Software Migration;legacy software migration;cloud computing;risk management;Business;Europe;time reduction;software modernization management;cloud-based software;Software Modernization;Biological system modeling;technical aspect;model-driven engineering;Cloud Computing;MDE;risk reduction;forward engineering;ARTIST;reverse engineering;software maintenance;Reverse engineering;business model;operating software;software delivery;Model-driven Engineering;cost reduction
##Overview
This paper is a fishing paper.It proposes an approach with future evaluationNevertheless is also cited (by other fishing papers) This project defines a process. The process is called to be iterative, since it has some feedback arrow. Never the less, there is not much relating between the claims and real code.

##References
###This article is cited by
 Daniel Escobar, Diana Cárdenas, Rolando Amarillo, Eddie Castro, Kelly Garcés, Carlos Parra, Rubby Casallas, "Towards the understanding and evolution o
 Khadija Sabiri, Fouzia Benabbou, Hicham Moutachaouik, Mustapha Hain, "Towards a cloud migration framework", (https://scholar.google.com/scholar?as_q=T
 Eliot Salant, Philipp Leitner, Karl Wallbom, James Ahtes, "A framework for a cost-efficient cloud ecosystem", (https://scholar.google.com/scholar?as_q
 Martyn Ellison, Radu Calinescu, Richard Paige, "Re-engineering the Database Layer of Legacy Applications for Scalable Cloud Deployment", (https://scho
 Mauricio Verano, Lorena Salamanca, Mario Villamizar, Oscar Garces, Angee Zambrano, Carlos Valencia, Rubby Casallas, Lina Ochoa, Harold Castro, Santiag
 Alexander Bergmayr, Manuel Wimmer, Gerti Kappel, Michael Grossniklaus, "Cloud Modeling Languages by Example", (https://scholar.google.com/scholar?as_q
 Johanna Ullrich, Tanja Zseby, Joachim Fabini, Edgar Weippl, "Network-Based Secret Communication in Clouds: A Survey", (https://scholar.google.com/scho
 Claudia Raibulet, Francesca Arcelli Fontana, Marco Zanoni, "Model-Driven Reverse Engineering Approaches: A Systematic Literature Review", (https://sch
 Alexander Bergmayr, Hugo Bruneliere, Jordi Cabot, Jokin Garcia, Tanja Mayerhofer, Manuel Wimmer, "fREX: fUML-based Reverse Engineering of Executable B
###This article Cites
 B. Selic, "MDA Manifestations," UPGRADE,
 G. Canfora, M. D. Penta, and L. Cerulo, 
 M. L. Badger, T. Grance, R. Patt-Corner,
 M. Harman, K. Lakhotia, J. Singer, D. R.
 J. Bézivin, "On the Unification Power of

##Article Metadata
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=6498212
isNow false
subType IEEE Conference
purchaseOptions a Dictionary('mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfo'->an OrderedCollection(a Dictionary('memberPrice'->'$14.95' 'nonMemberPrice'->'$33.00' 'partNumber'->'6498512' 'type'->'PDF/HTML' )) 'pdfPricingInfoAvailable'->true 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
conferenceDate 5-8 March 2013
pubTopics an OrderedCollection(a Dictionary('name'->'Computing and Processing' ))
pdfUrl nil
isNumber 6498434
isPromo false
startPage 465
isEphemera false
isACM false
isSAE false
mlTime PT0.053883S
isMorganClaypool false
sourcePdf 4948a465.pdf
isFreeDocument false
isDynamicHtml true
pdfPath /iel7/6498212/6498434/06498512.pdf
articleId 6498512
isMarketingOptIn false
isOpenAccess false
isSMPTE false
chronOrPublicationDate 5-8 March 2013
isEarlyAccess false
isnn an OrderedCollection(a Dictionary('format'->'Print ISSN' 'value'->'1534-5351' ))
mediaPath /mediastore_new/IEEE/content/media/6498212/6498434/6498512
endPage 468
isStaticHtml true
confLoc Genova, Italy
isGetAddressInfoCaptured false
content_type Conferences
_value IEEE
htmlLink /document/6498512/
accessionNumber 13446513
html_flag true
lastupdate 2020-09-13
pubLink /xpl/conhome/6498212/proceeding
isChapter false
isStandard false
isCustomDenial false
publicationYear 2013
htmlAbstractLink /document/6498512/
issueLink /xpl/tocresult.jsp?isnumber=6498434
displayPublicationTitle 2013 17th European Conference on Software Maintenance and Reengineering
isGetArticle false
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'true' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'true' 'footnotes'->'false' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
ml_html_flag true
openAccessFlag F
isJournal false
publicationNumber 6498212
dbTime 2 ms
rightsLinkFlag 1
contentType conferences
isBookWithoutChapters false
xploreDocumentType Conference Publication
chronDate 5-8 March 2013
isProduct false
metrics a Dictionary('citationCountPaper'->16 'citationCountPatent'->1 'totalDownloads'->613 )
citationCount 16
ephemeraFlag false
allowComments false
formulaStrippedArticleTitle Migrating Legacy Software to the Cloud with ARTIST
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=Migrating+Legacy+Software+to+the+Cloud+with+ARTIST&isbn=978-1-4673-5833-0&publicationDate=March+2013&author=Alexander+Bergmayr&ContentID=10.1109/CSMR.2013.73&orderBeanReset=true&startPage=465&endPage=468&proceedingName=2013+17th+European+Conference+on+Software+Maintenance+and+Reengineering
publicationTitle 2013 17th European Conference on Software Maintenance and Reengineering
isbn an OrderedCollection(a Dictionary('format'->'Electronic ISBN' 'isbnType'->'' 'value'->'978-0-7695-4948-4' ) a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'978-1-4673-5833-0' ))
xplore-pub-id 6498212
xplore-issue 6498434
isConference true
displayDocTitle Migrating Legacy Software to the Cloud with ARTIST
doiLink https://doi.org/10.1109/CSMR.2013.73
isOUP false
isNotDynamicOrStatic false
isBook false
getProgramTermsAccepted false
articleNumber 6498512
