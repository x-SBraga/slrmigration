
#Strategic directions in software engineering and programming languages
##Citing
ArticleAuthorRef: (81100274423@ACM;a ZoaArticleReference)996

##Authors
Auhor Name: Gunter, Carl
Auhor ID: 81100274423@ACM; 
Auhor Affiliations:  Univ. of Pennsylvania, Philadelphia; 
Auhor Name: Mitchell, John
Auhor ID: 81338490160@ACM; 
Auhor Affiliations:  Stanford Univ., Stanford, CA; 
Auhor Name: Notkin, David
Auhor ID: 81100636585@ACM; 
Auhor Affiliations:  Univ. of Washington, Seattle; 
##Keywords

##Overview
The article manfiests the relationship between programming language research and software engineering research. The article manifests how their research can be usefully used to feedback.AST by example are used for compilers and also for software analysis. 

##References
###This article is cited by
 Proceedings of the Conference on The Future of Software Engineering (10.1145/336512.336556)
 Local Compilation: A Novel Paradigm for Multilanguage-Based and Reliable Distributed Computing over the Internet (10.1177/003754970007500103)
 The 7th International Conference on Advanced Communication Technology. (http://ieeexplore.ieee.org/document/1462879/)
 http://www.crcnetbase.com/doi/abs/10.1201/9781420031102.ch7 (abs/10.1201/9781420031102.ch7)
 Proceedings of the 11th international conference on Advanced Communication Technology - Volume 3 (10.5555/1701655.1701814)
 A framework for specifying and verifying the behaviour of open systems (http://linkinghub.elsevier.com/retrieve/pii/S1567832604000372)
###This article Cites
 ABADI, M., LAMPSON, B., AND LEVY, J.-J. 
 CRAIGEN, D. H., GERHART, S. L., AND RALS
 HORWITZ, S., REPS, T., AND BINKLEY, D. 1
 ALSPAUGH, T. A., FAULK, S. R., BRITOON, 
 PARNAS, D. L. 1994. Software aging. In P
 MURPHY, G. C. AND NOTKIN, D. 1995. Light
 GAMMA, E., HELM, R., JOHNSON, R., AND VL
 WING, J. 1990. A specifier's introductio
 LEHMAN, M.M. 1980. Programs, life cycles
 CALLAHAN, D. AND KENNEDY, K. 1988. Analy
 CHAMBERS, C., DEAN, J., AND GROVE, D. 19
 CHIKOFSKY, E. AND CROSS, J. 1990. Revers
 GONG, L., LINCOLN, P., AND RUSHBY, J. 19
 MURPHY, G. C., NOTKIN, D., AND LAN, E. S
 VANINWEGEN, M. 1996. The machine-assiste
 HAYES, B. 1995. Waiting for 01-01-00. Am
 CLARKE, E. M., GERMAN, S. M., AND HALPER
 CRAIGEN, D. H., GERHART, S. L., AND RALS
 ATKINSON, D. C. AND GRISWOLD, W. G. 1996
 SCHERLIS, W. 1994. Boundary and path man

##Article Metadata
Source Dec. 1996
EventType ARTICLE
pages an OrderedCollection(727)
issue 4
PublisherCity New York, NY, USA
volume 28
isnn 0360-0300
NumberOfPages 11
