
#Network-centric migration of embedded control software: a case study
##Citing
ArticleAuthorRef: (81100489113@ACM;a ZoaArticleReference)3

##Authors
Auhor Name: Souza, Phillip de
Auhor ID: 81100489113@ACM; 
Auhor Affiliations:  Department of Computer Science, University of Victoria, B.C., Canada; 
Auhor Name: McNair, Andrew
Auhor ID: 81100110932@ACM; 
Auhor Affiliations:  Department of Computer Science, University of Victoria, B.C., Canada; 
Auhor Name: Jahnke, Jens H.
Auhor ID: 81100587167@ACM; 
Auhor Affiliations:  Department of Computer Science, University of Victoria, B.C., Canada; 

##Abstract
Over the last two decades, microcontrollers have replaced conventional electronics in the control of most mechatronic devices in use today. Recently, we have seen the beginning of a new technological movement that aims towards using the Internet for integrating embedded devices to form pervasive computing infrastructures. Smart Spaces, tele-control and Business-To-Machine (B2M) eCommerce are among the emerging technologies currently under research and development.In a collaborative project with industry and the Herzberg Institute of Astrophysics, we have investigated tools and techniques that aid the migration of existing embedded control software to such network-centric environments. The goal is to be able to inexpensively leverage existing products to modern applications rather than having to re-implement highly specialized embedded programs.This paper reports on our experiences with a case study on migrating a real-world micro-controller application to a networked infrastructure. Based on our experiences, we propose a method that would help practitioners tackle similar reengineering projects.
##Keywords

##Overview
This article introduces a method / process  on migration of an embedded system for telescope contorlling to network centric. It is a great article, since it includes problematics on hardware design, hardware adaptation, therefore on compiling, language, libraries etc. The proposed process is composed by - Understand how the legacy embedded system has been used.- Plan the usage of the system when it has been transformed to a network-centric component.- Evaluate hardware and upgrade if necessary.  - Reverse engineer the legacy code  - Classify the components in the legacy code. - Place the gates- Integrate the delivery network service.It includes during the experiments and project, some parts on knowledge adquirance: this is something oftenly neglected ,specially from the point of view of usage. This migration deals with a really large contexts of migration: hardware migration / adaption, runtime migration,  compiler migration, and software adaption to the new features. 

##References
###This article is cited by
 {14} J. Howard Johnson et al M. Whitney, K. Kontogiannis. Using an integrated toolset for program understanding. CAS Conference Proceedings, CASCON, 1
 {2} W. Boggs and M. Boggs. Mastering UML with Rational Rose. SYBEX, Alameda, CA, 1999. (10.5555/560091)
 {5} M. Nagl W. Schafer G. Engels, C. Lewerentz and A. Schurr. Building integrated software development environments part i: Tool specification. New Yo
 {11} Umit Kiziloglu. Ccd operation - quick reference guide. http://www.tug.tubitak.gov.tr/aletler/, 1 2000. (http://scholar.google.com/scholar?hl=en&q
 {19} Omondo. Eclipseuml home page. http://www.eclipseuml.com, 6 2002. (http://scholar.google.com/scholar?hl=en&q=%7B19%7D+Omondo.+Eclipseuml+home+page
 {21} UBC SPL Project. the software practices lab. http://www.cs.ubc.ca/labs/spl/aspectc.html, 6 2003. (http://scholar.google.com/scholar?hl=en&q=%7B21
 {18} University of Waterloo SWAG Group. Cppx home page. http://swag.uwaterloo.ca/cppx/, 05 2003. (http://scholar.google.com/scholar?hl=en&q=%7B18%7D+U
 {8} Intec Automation Inc. microcommander home page. http://www.microcommander.com/, 6 2003. (http://scholar.google.com/scholar?hl=en&q=%7B8%7D+Intec+A
 {9} Scientific Toolworks Inc. Understand for c++. http://www.scitools.com/ucpp.html, 05 2003. (http://scholar.google.com/scholar?hl=en&q=%7B9%7D+Scien
 {16} University of Victoria CHISEL Group. Chisel home page. http://shrimp.cs.uvic.ca/chisel.htm, 2001. (http://scholar.google.com/scholar?hl=en&q=%7B1
 {22} Ying Zou and Kostas Kontogiannis. Quality driven transformation compositions for object oriented migration. IEEE Asia Pacific Software Engineerin
 {1} Danilo Beuche, Wolfgang Schrder-Preikschat, Olaf Spinczyk, and Ute Spinczyk. Streamlining object-oriented software for deeply embedded application
 {4} Eclipse. Eclipse home page. http://www.eclipse.org/, 05 2003. (http://scholar.google.com/scholar?hl=en&q=%7B4%7D+Eclipse.+Eclipse+home+page.+http%
 {20} The AspectC++ Project. Aspectc++ download page. http://www.aspectc.org/download.html, 6 2003. (http://scholar.google.com/scholar?hl=en&q=%7B20%7D
 {7} University of Hawai'i Institute for Astronomy Hubert Yamada. Nextstep ccd-camera control system. http://ccd.ifa.hawaii.edu/uh88/ccd.php, 6 2003. (
 {10} K. Wong et al J. Mylopoulos, M. Stanley. Towards an integrated toolset for program understanding. CAS Conference Proceedings , CASCON, 1994. (10.
 {6} K. Wong H. A. Muller and S. R. Tilley. Understanding software systems using reverse engineering technology. The 62nd Congress of L'Association Can
 {17} University of Victoria Rigi Group. Rigi home page. http://www.rigi.csc.uvic.ca, 05 2003. (http://scholar.google.com/scholar?hl=en&q=%7B17%7D+Univ
 {13} K. Wong M.-A.D. Storey and H. A. Muller. How do program understanding tools affect how programmers understand programs? Science of Computer Progr
 {15} Government of Canada National Research Council. Herzberg institute of astrophysics welcome page. http://www.hiaiha.nrc-cnrc.gc.ca, 6 2003. (http:
 {3} M. d'Entremont and J.H. Jahnke. microsynergy - generative tool support for networking embedded controllers. Toronto:ACM Press, NCC'01, 2001. (http
 {12} H. A. Muller M.-A. D. Storey and K. Wong. Manipulating and documenting software structures. Series on Software Engineering and Knowledge Engineer
###This article Cites
 {14} J. Howard Johnson et al M. Whitney,
 {2} W. Boggs and M. Boggs. Mastering UML
 {5} M. Nagl W. Schafer G. Engels, C. Lew
 {11} Umit Kiziloglu. Ccd operation - qui
 {19} Omondo. Eclipseuml home page. http:
 {21} UBC SPL Project. the software pract
 {18} University of Waterloo SWAG Group. 
 {8} Intec Automation Inc. microcommander
 {9} Scientific Toolworks Inc. Understand
 {16} University of Victoria CHISEL Group
 {22} Ying Zou and Kostas Kontogiannis. Q
 {1} Danilo Beuche, Wolfgang Schrder-Prei
 {4} Eclipse. Eclipse home page. http://w
 {20} The AspectC++ Project. Aspectc++ do
 {7} University of Hawai'i Institute for 
 {10} K. Wong et al J. Mylopoulos, M. Sta
 {6} K. Wong H. A. Muller and S. R. Tille
 {17} University of Victoria Rigi Group. 
 {13} K. Wong M.-A.D. Storey and H. A. Mu
 {15} Government of Canada National Resea
 {3} M. d'Entremont and J.H. Jahnke. micr
 {12} H. A. Muller M.-A. D. Storey and K.

##Article Metadata
EventType PAPER_CONFERENCE
pages an OrderedCollection(54)
EventCity Toronto, Ontario, Canada
CollectionTitle CASCON '03
NumberOfPages 12
pdfUrl https://dl.acm.org/doi/pdf/10.5555/961322.961333
