
#Developing legacy system migration methods and tools for technology transfer
##Citing
ArticleAuthorRef: (Andrea De Lucia@WILEY;a ZoaArticleReference)8

##Authors
Auhor Name: De Lucia, Andrea
Auhor ID: Andrea De Lucia@WILEY; 
Auhor Affiliations:  Dipartimento di Matematica e Informatica, University of Salerno, Via Ponte Don Melillo, 84084 Fisciano (SA), Italy; 
Auhor Name: Francese, Rita
Auhor ID: Rita Francese@WILEY; 
Auhor Affiliations:  Dipartimento di Matematica e Informatica, University of Salerno, Via Ponte Don Melillo, 84084 Fisciano (SA), Italy; 
Auhor Name: Scanniello, Giuseppe
Auhor ID: Giuseppe Scanniello@WILEY; 
Auhor Affiliations:  Dipartimento di Matematica e Informatica, University of Basilicata, Viale Dell'Ateneo, Macchia Romana, 85100 Potenza, Italy; 
Auhor Name: Tortora, Genoveffa
Auhor ID: Genoveffa Tortora@WILEY; 
Auhor Affiliations:  Dipartimento di Matematica e Informatica, University of Salerno, Via Ponte Don Melillo, 84084 Fisciano (SA), Italy; 

##Abstract
Abstract This paper presents the research results of an ongoing technology transfer project carried out in cooperation between the University of Salerno and a small software company. The project is...
##Keywords
migration to the Web;reengineering;technology transfer;legacy systems
##Overview
The article exposes an extended and exhaustive study over a large migration experimentThe migration aims to bring an existing software developed as client / server to a web-based environment.The origin language is ACUCOBOL. The destination web environment is java JSP. The process goes: Assessing the current systemDefining the target system Identifying risks and problemsThe migration processEvaluationThe target system responds to a web architecture that delegates the business logic to a wrapped legacy backend.This backend is properly wrapped by a middleware platform specialized in wrapping.For being able to be wrapped, the legacy system has been dramatically modified from interactive UI to batch. Once in batch mode, the middleware offers primitives that allow the process to be orchestrated: wait, displayCobol (equivalent to display but remote), and set/get/clear shared memory. The middleware also proposes some primitives for the JSP endpoint: startCobol, sync (couterpart of wait) and the access to shared memory: set/get/create. Finally the UI code (defined in the SCREEN section in  cobol) is parsed analysed and used to generate the JSP pages.The article proposes a serious evaluation, adding metrics a over the precision, the recogniced risks and the success.

##References
###This article is cited by
 Supriya Pulparambil, Youcef Baghdadi, Abdullah Al-Hamdani, Mohammed Al-Badawi, Exploring the main building blocks of SOA method: SOA maturity model pe
 Domenico Amalfitano, Anna Rita Fasolino, Porfirio Tramontana, Vincenzo De Simone, Giancarlo Di Mare, Stefano Scala, A Reverse Engineering Process for 
 Uolevi Nikula, Christian Jurvanen, Orlena Gotel, Donald C Gause, Empirical validation of the Classic Change Curve on a software technology change proj
 H. M. Sneed, K. Erdoes, undefined, 2013 17th European Conference on Software Maintenance and Reengineering, 10.1109/CSMR.2013.32, (231-240), (2013). C
 S. Ratnajeevan H. Hoole, T. Arudchelvam, undefined, 2009 International Conference on Industrial and Information Systems (ICIIS), 10.1109/ICIINFS.2009.
 Giuseppe Scanniello, Ugo Erra, Giuseppe Caggianese, Camine Gravino, On the Effect of Exploiting GPUs for a More Eco-Sustainable Lease of Life, Interna
 Zoltán Tóth, László Vidács, Rudolf Ferenc, Comparison of Static Analysis Tools for Quality Measurement of RPG Programs, Computational Science and Its 
 Helton Emanuel Reis Santana, Glauco de Figueiredo Carneiro, undefined, 2014 11th International Conference on Information Technology: New Generations, 
 Nader Kesserwan, Rachida Dssouli, Jamal Bentahar, Modernization of Legacy Software Tests to Model-Driven Testing, Emerging Technologies for Developing
 Cristian Mateos, Marco Crasso, Juan M. Rodriguez, Alejandro Zunino, Marcelo Campo, Measuring the impact of the approach to migration in the quality of
 Borting Chen, Ho-Pang Hsu, Yu-Lun Huang, Bringing Desktop Applications to the Web, IT Professional, 10.1109/MITP.2016.15, 18 , 1, (34-40), (2016). Cro
 undefined, Proceedings of the 9th International Conference on Utility and Cloud Computing - UCC '16, 10.1145/2996890.2996908, (43-48), (2016). Crossre
 undefined, Proceedings of the 8th Asia-Pacific Symposium on Internetware - Internetware '16, 10.1145/2993717.2993718, (1-10), (2016). Crossref (10.114
 Ravi Khadka, Amir Saeidi, Andrei Idu, Jurriaan Hage, Slinger Jansen, Legacy to SOA Evolution, Migrating Legacy Applications, 10.4018/978-1-4666-2488-7
 Andrea De Lucia, Fausto Fasano, Genoveffa Tortora, Giuseppe Scanniello, Does software error/defect identification matter in the Italian industry?, IET
 Ravi Khadka, Amir Saeidi, Slinger Jansen, Jurriaan Hage, Geer P. Haas, undefined, 2013 20th Working Conference on Reverse Engineering (WCRE), 10.1109/
 Ravi Khadka, Amir Saeidi, Slinger Jansen, Jurriaan Hage, undefined, 2013 IEEE 7th International Symposium on the Maintenance and Evolution of Service-
 Harry M. Sneed, A pilot project for migrating COBOL code to web services, International Journal on Software Tools for Technology Transfer, 10.1007/s10
 Marco Torchiano, Massimiliano Di Penta, Filippo Ricca, Andrea De Lucia, Filippo Lanubile, Migration of information systems in the Italian industry: A 
 Michael Wahler, Raphael Eidenbenz, Carsten Franke, Yvonne-Anne Pignolet, undefined, 2015 IEEE International Conference on Software Maintenance and Evo
 C. Arevalo, I. Ramos, J. Gutiérrez, M. Cruz, Practical Experiences in the Use of Pattern-Recognition Strategies to Transform Software Project Plans in
 C. Zillmann, A. Winter, A. Herget, W. Teppe, M. Theurer, A. Fuhr, T. Horn, V. Riediger, U. Erdmenger, U. Kaiser, D. Uhlig, Y. Zimmermann, undefined, 2
 Ravi Khadka, Gijs Reijnders, Amir Saeidi, Slinger Jansen, Jurriaan Hage, undefined, 2011 27th IEEE International Conference on Software Maintenance (I
 Maria Caulo, Rita Francese, Giuseppe Scanniello, Antonio Spera, Does the Migration of Cross-Platform Apps Towards the Android Platform Matter? An Appr
 G. Scanniello, U. Erra, G. Caggianese, C. Gravino, undefined, 2013 17th European Conference on Software Maintenance and Reengineering, 10.1109/CSMR.20
 Samuel Ratnajeevan Herbert Hoole, Thiruchelvam Arudchelvam, Janaka Wijayakulasooriya, Reverse Engineering Legacy Finite Element Code, Materials Scienc
 Juan Rodriguez, Marco Crasso, Cristian Mateos, Alejandro Zunino, Marcelo Campo, Bottom-up and top-down COBOL system migration to Web Services: An expe
 Andrea De Lucia, Massimiliano Di Penta, Filippo Lanubile, Marco Torchiano, undefined, 2009 13th European Conference on Software Maintenance and Reengi
###This article Cites

##Article Metadata
pages an OrderedCollection(1333 1334 1335 1336 1337 1338 1339 1340 1341 1342 1343 1344 1345 1346 1347 1348 1349 1350 1351 1352 1353 1354 1355 1356 1357 1358 1359 1360 1361 1362 1363 1364)
pdfUrl https://onlinelibrary.wiley.com/doi/pdf/10.1002/spe.870
issue 13
volume 38
journal Software: Practice and Experience
isnn 1097-024X
AbstractUrl https://onlinelibrary.wiley.com/doi/abs/10.1002/spe.870
Language en
